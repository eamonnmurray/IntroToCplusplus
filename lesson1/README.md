Introduction to C++: Lesson 1
=============================

Topics
------

1. [Compiling and Makefiles](#11-compiling-and-makefiles)
2. [IO and Strings](#12-io-and-strings)

1.1 Compiling and Makefiles
---------------------------

Enter the 1.1_Compiling directory: `cd 1.1_Compiling`, and open the file
`hello.cpp` in vim or your editor of choice: `vim hello.cpp`. The contents are
as follows:

```c++
// A short C++ program to greet the world.
#include <iostream>

int main()
{
    std::cout << "Hello world!" << std::endl;
    return 0;
}
```
To compile this code we can do

```bash
g++ hello.cpp
```

This will generate an executable called `a.out`. We can run this by typing
`./a.out`. It's nice to be able to pick the executable name ourselves though.
We can do this by adding the option `-o executablename` to g++, i.e.

```bash
g++ hello.cpp -o hello.x
```

Note: by convention I'm going to use the extension .x for executables
throughout the course. It's possible to set this to whatever you like, or more
commonly, not use any extension. Additionally we'll use the `.cpp` extension
for C++ source code. This is probably the most commonly used extension,
although other common variations include `.cc`, `.C`, `.cxx` and `.c++`.

### Make
While typing out the g++ line to compile the code from source as above isn't
particularly time consuming, entering the compilation instructions by hand
can become quite complicated as projects expand to include more source files,
and if you'd like to pass other options to the compiler. For this reason it's
useful to use [make](https://www.gnu.org/software/make/). This reads a
makefile containing a set of instructions for compiling the executable from
the various source files.

The most basic example is given in Makefile01. The contents are as follows:

```makefile
# This is an example of the most basic Makefile you can have.
# Lines starting with # are comments.

# Makefiles list on one line a "target" followed a colon, along with its
# dependencies. The following lines are tab intended and give a set of
# commands to produce the target.

# Here we define a target called all. When `make` is invoked without a
# target name, it builds the first target that appears in the makefile.
# Typically the symbolic target "all" is used for this and represents
# building the full code.
# Note we haven't listed any dependencies for all here.

all:
	g++ hello.cpp -o hello.x

# The line following the target, gives (following a tabspace) the sytem
# command to compile the code.
# We call g++ with the name of the source file, and we also use the -o flag
# to allow us to specify the name of the resulting executable. This can
# be omitted, in which case the executable will be called a.out.
```

We can't yet type `make` and have our code be compiled as there is no
`Makefile` in this directory. We will need to do **one** of the following:

- Copy Makefile01 to Makefile: `cp Makefile01 Makefile`. Then we can run
 `make`.
- Make a symbolic link from Makefile01 to Makefile:
 `ln -s Makefile01 Makefile`. Then we can run `make`.
- Use the `-f` flag with `make` to specify the name of the makefile to use:
 `make -f Makefile01`.

Every time we run `make`, it will execute the specified g++ command. Now that
the basic idea has been outlined we can start to look at build towards more
advanced features. Lets take a look at `Makefile02`:

```makefile
# This time as well as a target, we a dependency. This tells make, that the
# default operation (first target) is satisfied when `hello.x` exists.
all: hello.x

# Now we list a target hello.x. This is useful since if hello.x already exists
# make can understand it doesn't need to recompile it.
hello.x:
	g++ hello.cpp -o hello.x
```

If we use `Makefile02` with make, the code is compiled as before, but now if we
try to repeat the make process, it automatically sees that `hello.x` already
exists, so the dependencies for the "all" target are satisfied and there's no
more for it to do. Try this yourself.

It's useful though to be able to force it to recompile our code from the
beginning. The standard way this is done is by defining a "clean" target which
removes the executable and (or sometimes just) intermediate files on which the
executable depends. This is done by adding an `rm` command instead of a `g++`
command as shown in `Makefile03`:

```makefile
all: hello.x

# Also it's useful to set the source as a dependency. Now if you edit the cpp
# file, so that its modification time is more recent than the executable,
# "make" will understand that "hello" needs to be recompiled.
hello.x: hello.cpp g++ hello.cpp -o hello.x

# It's useful to be able to force a full recompile. The standard way to
# do this is to create a target called "clean" which removes the executable
# and any intermediate files.
clean:
	rm hello.x
```

Using this makefile and specifying `make clean` will now run the `rm` command
specified.

We can also define variables in makefiles. This is useful as, for example, it
allows us to set the compiler and compiler flags in one place rather than
entering it explicitly for each file that needs to be compiled. This is shown
in `Makefile04`:

```makefile
# Rather than setting the compiler explicitly in the system command line
# following the target, it's useful to use variables. In this example we set
# both the c++ compiler and flags using variables.

# Use the g++ compiler
CC = g++

# Set the compiler flags. Lets turn on additional compiler warnings. This
# pair of options are really useful for flagging potential problems in your
# source.
CFLAGS = -Wall -Wextra

all: hello.x

hello.x: hello.cpp
	$(CC) $(CFLAGS) hello.cpp -o hello.x

clean:
	rm hello.x
```

If you have more than one source file, it's useful to separate out the compile
step from the linking step. The compile step generates an object file from a
source file, and the linking step puts all the object files together into an
executable. This is shown in `Makefile05`:

```makefile
# So far we've been generating the executable in just one step. This works fine
# when there is just one source file, but more complicated projects will
# have the source divided up into several files, each of which must be
# compiled to an object file, before being linked together to an executable.

# Use the g++ compiler
CC = g++

# The -c flag tells the compiler to generate an object file.
CFLAGS = -c -Wall -Wextra

# Set the linker flags. Note it's fine to leave variables blank.
LDFLAGS =

all: hello.x

# Now we say that the hello executable depends on the hello.o object file.
# This is the linking step, so we use LDFLAGS rather than CFLAGS.
hello.x: hello.o
	$(CC) hello.o -o hello.x $(LDFLAGS)

# And we set a hello.o target which depends on the source file.
hello.o: hello.cpp
	$(CC) $(CFLAGS) hello.cpp

# We should also update the "clean" target to remove object files.
clean:
	rm hello.x hello.o
```

Now we can make a more general purpose makefile, in particular, making full use
of variables. This is shown in `Makefile06`:

```makefile
# We can make more extensive use of variables in both the target and
# dependency lists to make a more general purpose Makefile. Here we make use
# of the $@ variable which resolves to the name of the target currently being
# processed, and the $< variable which contains the first dependency listed.

# While this may look quite complicated, it breaks down into setting variables
# defining the compiler, flags, source and executable names, and then listing
# a set of recipes to compile and put everything together.

# Use the g++ compiler
CC = g++

# Set the compiler flags. These turn on additional compiler warnings.
CFLAGS = -c -Wall -Wextra

# Set the linker flags.
LDFLAGS =

EXECUTABLE = hello.x
# Any other sources could be listed here too, separated by spaces.
SOURCES = hello.cpp
# This automatically generates a list of object names by substituting .cpp
# wih .o
OBJECTS = $(SOURCES:.cpp=.o)

all: $(SOURCES) $(EXECUTABLE)

# This says the executable depends on all the objects, so make will check
# whether all the objects are compiled first. And following this is the
# line defining how to link all the object files together. The $@ substitutes
# in the name of the target, so we could use $(EXECUTABLE) instead here.
$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

# Now we define are rule to make an object file from a source file. The %<
# substitutes in the name of the dependency.
%.o: %.cpp
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm $(EXECUTABLE) $(OBJECTS)
```
We have used two special (automatic) variables in this example: `$@` and `$<`.
A number of these are available. The most commonly used are:

- `$@` contains the target of the rule.
- `$<` contains the first dependency listed.
- `$^` contains all the dependencies listed.
   - In the last example, we could use either `$^` or `$<` in the build
     command for `%.o`, and in the build command for `$(EXECUTABLE)` we could
     use `$^` instead of `$(OBJECTS)`.
- `$?` contains all dependencies that are newer than the target.

In future we'll be modifying this makefile to compile the other examples in
this course. There are many more advanced features of make that we won't be
covering. Full documentation for the GNU implementation of make (the default
version on Linux) is available
[online](https://www.gnu.org/software/make/manual/).

**Note**
- For larger projects where you have multiple source files, with some
  non-trivial dependency relation between them, it is better to have these
  dependencies explicitly laid out in your makefile. This will ensure the
  correct set of files are recompiled when you make some modifications. The
  examples given above are too simple for this situation. If you don't do
  this correctly, your code may compile but crash when you try to run it.
- You will also need to ensure that you don't have any circular dependencies,
  where some source file depends on a file that directly or indirectly depends
  on it. You need to reorganise your code in this case as `make` will not
  be able to compile your code.

### The source file

Now that we've covered how to compile our code, let's look in more detail
at the source file. An annotated version of the hello.cpp file is in
`hello_annotated.cpp`:

```c++
// A short C++ program to greet the world.

// Lines starting with // are treated as comments.

/* Multi-line comments can be created
   like this. */

#include <iostream>
/* Lines starting with # are processed first.
 * #include tells the compiler to include a header file. These contain
 * declarations of functions and variables in the associated library.
 * The angle brackets <> indicate "iostream" is part of the standard library
 * that will be provided with any C++ compiler. */

int main()
/* Here we define a function called main.
 * Every C++ program must have a main function.
 * Functions return values of a particular type, in this case we have stated
 * "main" will return an integer.
 * The parentheses are where any arguments to the function would be located. In
 * this case we say that the main function takes no arguments. */

{
    // The code associated with the function is enclosed in braces {}.

    std::cout << "Hello world!" << std::endl;
    /* This line causes the string "Hello world!" to be printed to the
     * standard output stream, followed by a newline.
     * Note the ; at the end here.
     * All C++ statements must end with a semi-colon. */

    return 0;
    /* We said the function main would return an integer, and we do so here.
     * The return statement causes the function to end and the associated
     * value to be passed back to whatever called the function.
     * 0 is generally used to mean the code has completed successfully.
     * Other values are typically used to indicate various types of errors
     * have occurred. */
}
```

### Namespaces

C++ also makes use of namespaces. How these work are described in
`hello2_annotated.cpp`:

```c++
// A short C++ program to greet the world.

#include <iostream>
using namespace std;
/* C++ uses namespaces. A namespace is used to collect together a group of
 * related variables and helps avoid clashes in the names of very different
 * items. The namespace an item is in is indicated by using
 * namepace_name::item_name as we had previously with std::cout.
 * The above statement allows us to import the std namespace into the current
 * scope. This means we no longer nead to prefix items from the std namespace
 * with std::. */

int main()
{
    cout << "Hello world!" << endl;
    // Note we have dropped the std:: before cout and endl here.

    return 0;
}
```

We'll be using the std namespace throughout this course.

### Coding style

This last point to note is coding style. Take a look at `hello3.cpp`:

```c++
// C++ doesn't take whitespace or newlines into account. The following code
// is perfectly valid, but much more difficult to read.
// It's considered good practice to have the layout and indentation reflect
// the logical structure of the code, and it will make your code much easier
// to work with.
// Establishing good habits now will save you a lot of time in the future.
#include<iostream>
using namespace std;int main(){cout<<"Hello world!"<<endl;return 0;}
```

It's very important to make the effort to make your code easy to read and
follow. Here are some simple guidelines to start with:

- Use frequent comments
    - Put a comment at the beginning of any code or function (covered later)
      outlining what it does.
    - Describe what you're trying to do in a particular section.
    - Clarify any choices you have made where the reason is not immediately
      apparent.
- Use meaningful variable names.
    - E.g. a variable used to store the users age could be called `user_age`.
    - A variable used to store a photon energy could be called `E_photon`.
    - If you read back through your code and find yourself needing to check
      other sections to figure out what information is stored in a variable,
      you should consider renaming it.
- Use empty lines to separate sections of code, similarly to using
  paragraphs for written text.
- We'll cover functions later in this course, and once you understand them
  you should use them frequently. These naturally break up code into
  individually understandable chunks.

### [Exercises](../exercises/README.md#11-compiling-and-makefiles)

Please complete the exercises listed in
[section 1.1](../exercises/README.md#11-compiling-and-makefiles) of the file
`exercises/README.md`.

1.2 IO and Strings
------------------

Now lets go to the directory `1.2_IO_Strings`. If you are in the
`1.1_Compiling` directory this would be done by typing `cd
../1.2_IO_Strings`. In this directory we have an example showing several ways
the "Hello world!" string can be output, as well as an example of how data can
be read into a variable:

```c++
// A C++ program giving examples of how strings can be output
#include <iostream>
using namespace std;

int main()
{
    string str1;
    string str2 = "world!";
    // The string type is defined in the standard library. If we didn't
    // specify we are using the std namespace above we would need to write
    // this as "std::string str1;".
    //
    // Variables don't strictly need to be declared at the start of the
    // function. We can assign a value to a variable as we declare as is done
    // for str2 here.

    cout << "1. Hello world!" << endl;
    // C++ uses data streams for input and output. The operator << sends
    // data to an output destination.

    cout << "2. " << "Hello " << "world!" << endl;
    // Repeated uses of << appends items to the stream.

    cout << "3. Hello ";
    cout << "world!" << endl;
    // endl is a constant marking the end of a line.

    str1 = "Hello";
    cout << "4. " << str1 << " " << str2 << endl;
    // We can use the string variables we defined earlier.

    str1 = "Hi world!";
    cout << "5. " << str1 << endl;
    // And we can redefine variables as we desire.

    cout << "Please enter a word and press enter: ";
    cin >> str1;
    // cin is defined in the standard library as standard input, and the
    // >> operator is used to read in input data.
    // You might notice if you enter text with any spaces, only the first
    // word is output.
    cout << "The word you entered was: ";
    cout << str1 << endl;

    return 0;
}
```

The `cin >> str1` construction allows us to read a single item entered by the
user. We will cover the `getline` command later in this course which can be
used to read a full line of data from stdin.

### [Exercises](../exercises/README.md#12-io-and-strings)

Please complete the exercises listed in
[section 1.2](../exercises/README.md#12-io-and-strings) of the file
`exercises/README.md`.
