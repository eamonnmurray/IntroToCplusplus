// Example showing how to use classes containing functions.
#include <iostream>
#include <string>

using namespace std;

class Atom {
    public:
        int atomic_number;
        double mass_number;
        double radius_pm;
        string symbol;

        // We add a function here that will output the data stored in the
        // class. Note we don't need an argument here, since the function
        // has access to the class variables directly. Note this function
        // is also public, so we can call it from the main function.
        void output_info() {
            cout << symbol << endl;
            cout << "Atomic number: " << atomic_number << endl;
            cout << "Mass number: " << mass_number << endl;
            cout << "Atomic radius (pm): " << radius_pm << endl;
        }
} ;

int main() {
    // Initialize two objects of the Atom class.
    Atom hydrogen, lithium;

    // Since the variables are public, we can set the variables contained in
    // each directly.
    hydrogen.atomic_number = 1;
    hydrogen.mass_number = 1.008;
    hydrogen.symbol = "H";
    hydrogen.radius_pm =  25;

    lithium.atomic_number = 3;
    lithium.mass_number = 6.941;
    lithium.symbol = "Li";
    lithium.radius_pm = 145;

    // The following construction calls the class function to output the data.
    hydrogen.output_info();
    cout << endl;
    lithium.output_info();

    return 0;
}
