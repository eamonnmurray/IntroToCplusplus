Introduction to C++: Exercises
==============================

Completing Exercises
--------------------

Each lesson will direct you to complete exercises listed here at the end of
each topic. Where an exercise directs you to create a code in its own
directory, you should make this directory within the exercises directory. Even
when not explicitly mentioned, your code should include a Makefile.

If tracking with git, I suggest creating a new branch for your solutions. Be
sure to remember to add each directory you create and any source files you
create, and commit regularly. If you fork the repository containing this course
on gitlab, be sure to keep it as a private repository so others won't be
tempted to copy your answer. You can give me guest access if you would like
feedback on your solutions.

If you find these too easy, I encourage to start working through the problems
at <https://projecteuler.net>.

Lessons
-------

1. [Compiling and Makefiles](#11-compiling-and-makefiles),
   [IO and Strings](#12-io-and-strings)
2. [Basic Types](#21-basic-types),
   [Mathematical Operations](#22-mathematical-operations)
3. [Conditions](#31-conditions),
   [Loops](#32-loops)
4. [Functions](#41-functions),
   [Pointers](#42-pointers)
5. [Static Arrays](#51-static-arrays),
   [Arguments to Main](#52-arguments-to-main)
6. [Dynamic Arrays](#61-dynamic-arrays),
   [Working With Files](#62-working-with-files)
7. [Data Structures](#71-data-structures),
   [Working With Larger Codes](#72-working-with-larger-codes)
8. [Using External Libraries](#81-using-external-libraries),
   [Classes](#82-classes)

1.1 Compiling and Makefiles
---------------------------

1. Compile the code with each of provided makefiles.
  1. In each case see what happens when you try to run `make` when the
     executable already exists.
  2. Test what happens when the executable already exists, but you update the
     timestamp on the source file using `touch hello.cpp`.
2. Make a directory `1.1_hello` and make a symbolic link to the file
   `../lesson1/1.1_Compiling/hello3.cpp` in this directory. Create a Makefile
   to compile `hello3.cpp` and confirm that it does compile and run as
   expected.

1.2 IO and Strings
------------------

1. Make a directory called `1.2_MyName`.
    1. In this directory create a C++ source code called `MyName.cpp` that when
       compiled and executed writes your name.
    2. Create a Makefile to compile this code to an executable called `MyName.x`.
    3. Confirm that your code compiles and runs correctly.

2.1 Basic Types
---------------

1. Make a copy of the source file in the example
   (`../lesson2/2.1_Types/variables_and_types.cpp`) in a directory called
   `2.1_VandT` and update it by including a comment after each type output
   line showing what was output.
2. Make a directory called `2.1_Age`. In this write a program and associated
   makefile that asks the user to input their year of birth as an integer,
   followed by their month of birth as a string, and then outputs this
   information again.
   1. What happens if the user enters the input in the opposite order to that
      requested?

2.2 Mathematical Operations
---------------------------

1. Make a directory called `2.2_AgeFromYear`. In it write a code and associated
   makefile that asks the user to enter their year of birth, and then output
   what age they will be at the end of this year.
2. Make a directory called `2.2_UnitConv`. In it write a code and associated
   makefile that asks the user to enter an energy in Hartree and outputs the
   energy converted to eV followed by the energy converted to J.

3.1 Conditions
--------------

1. In a directory called `3.1_SwitchToIf` write a code that produces the same
   output as the example `conditions2.cpp` using if statements instead of the
   switch statement.
2. In a directory called `3.1_OddOrEven` write a code that asks the user to
   input an integer and outputs whether than number is odd or even.

3.2 Loops
---------

1. In a directory called `3.2_FactorialWhile` write a program that asks to user
   to enter an integer, uses a while loop to calculate the factorial, and
   outputs the result.
2. In a directory called `3.2_FactorialFor` do the same thing but using a for
   loop.
3. In a directory called `3.3_Prime` write a program that asks the user to
   enter an integer and outputs whether or not that number is prime.

4.1 Functions
-------------

1. In a directory called `4.1_QuadRoots` make a copy of the example code
   `functions3.cpp` called `quad_roots.cpp`.
    1. Create a function called `b2_4ac` which returns the value of
       (b^2 - 4ac).
    2. Update the `quadratic_root1` function to use this new function.
    3. Create a `quadratic_root2` function which returns the lower root.
    4. Restructure the code to include an if statement which tests whether the
       quadratic has real roots. If it does, then return both roots. If it
       doesn't, then output an appropriate message and don't attempt to
       calculate them.

4.2 Pointers
------------

1. In a directory called `4.2_SwapInt` write a code that uses a function
   to swap the values of two integers using pointers.

5.1 Static Arrays
-----------------

1. In a directory called `5.1_DotProduct`, write a code that will read in
   two vectors (with three components) and output their dot product.
2. In a directory called `5.1_MatMul`, write a code that will read in two
   3x3 matrices and output the matrix given by their matrix product.

5.2 Arguments to Main
---------------------

1. In a directory called `5.2_ComplexMag`, write a code which takes the real
   and imaginary parts of a complex number as arguments and outputs the
   magnitude of the complex number.

6.1 Dynamic Arrays
------------------

1. In a directory called `6.1_VectorNorm`, take the code given in
   `../lesson5/5.1_Static_Arrays/arrays3.cpp` and modify it to use a dynamic
   array.
2. In a directory called `6.1_Stats`, write a code which takes an arbitray
   length list of real numbers as arguments and outputs the average and
   standard deviation.

6.2 Working With Files
----------------------

1. The directory `../lesson6/6.2_Files/DOS` contains a file containing many
   energy values. In a directory called `6.2_MinMax` make a symbolic link to this
   file. Write a code in this directory called `minmax.cpp` that will
   find the minimum and maximum energy value in the file.
2. In a directory called `6.2_DOS`, again make a symbolic link to the same file
   of energy values. Write a code called `dos.cpp` that will read the energy
   values and generate an output file called `si_dos.dat` which contains a
   histogram of these values with 100 divisions between -0.3 and 0.5.
    1. Generate a pdf plot of this resulting file in gnuplot.

7.1 Data Structures
-------------------

1. In a directory called `7.1_Complex` write a code in which a struct called
   `Complx` is defined to represent complex numbers, containing variables `re`
   for the real part, and `im` for the imaginary part. *Note there is a "class
   template" called `complex` in C++, but its use is outside the scope of this
   course. We're using the usual convention of capitalizing the first letter of
   user defined objects to avoid issues.*
    1. Write a function in this code that will take a Complx struct as an
       argument and return its absolute value.
    2. Also write a function that will take two of these Complx structs as
       arguments and return their product (the function will be of type
       Complx).
    3. The main function of the code should ask the user to input two complex
       numbers and then use the functions above to output the absolute value
       of each one followed by the product of the two input numbers.

7.2 Working With Larger Codes
-----------------------------

1.  Now in a directory `7.2_Complex` take the code you wrote for exercises in
    section 7.1 above and divide into into three files:
    1. A `.cpp` file containing the main function.
    2. A `.h` file containing the interfaces to the additional function
       definitions.
    3. A `.cpp` file containing these function definitions.

8.1 Using External Libraries
----------------------------

1. In a directory called `8.1_Inv` write a code which uses the armadillo
   library to output the inverse of a matrix read from a file named "M.dat".
   Output an error message if the matrix is not square. Refer to the armadillo
   documentation at http://arma.sourceforge.net/docs.html to find the
   functions you need.
2. In a directory called `8.1_Pi_MC` write a code which uses the random number
   generation provided by GSL (see
   https://www.gnu.org/software/gsl/manual/html_node/Random-Number-Generation.html
   for details).
    1. Generate random x and y coordinates both between 0 and 1. This will be
       repeated a user-input number of times.
    2. Keep count of the number of points that fall within the bounds of a the
       circle of radius 1 centred at the origin.
    3. Use this to estimate the area of the quarter circle, and hence estimate
       the value of pi.

8.2 Classes
-----------

1. In a directory called `8.2_Complex`, redo the exercise from the earlier
   section on data structures (7.1) and working with larger codes (7.2) to
   instead represent complex numbers using a class called `Complex`.
    1. Make the variables `re_` and `im_` private and include functions in the
       class to:
	1. Set both values.
	2. Return the real part.
	3. Return the imaginary part.
	4. Return the absolute value.
    2. Write a function external to the class that returns the product of two
       complex numbers (the function will be of type `Complex`) and use it to
       output the product of two user input complex numbers.
2. Now take the code and divide into into three files:
    1. A `.cpp` file containing the main function.
    2. A `.h` file containing the `Complex` class definition, including the
       interfaces to its internal functions, and the interface to the complex
       product function.
    3. A `.cpp` file containing these function definitions.

