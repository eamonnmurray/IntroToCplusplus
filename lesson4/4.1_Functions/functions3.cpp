// An example of the use of a function that returns values and has
// arguments, and is definied following main.
#include <iostream>
#include <cmath>
using namespace std;

// Here we define a function of type "double", this means it will return
// a double precision real number. And this function takes three doubles as
// arguments.
double quadratic_root1(double, double, double);
// This time we just define the interface here, and give the full function
// definition after the main function.

int main() {
    double user_a, user_b, user_c;

    cout << "Enter a, b and c for a quadratic equation (ax^2+bx+c=0): ";
    cin >> user_a;
    cin >> user_b;
    cin >> user_c;

    cout << quadratic_root1(user_a, user_b, user_c) << endl;

    return 0;
}

double quadratic_root1(double a, double b, double c) {
    // Return the larger root of a quadratic equation.

    double root1;

    root1 = -b + sqrt(pow(b, 2) - 4.0 * a * c);
    root1 = root1 / (2.0 * a);
    return root1;
}
