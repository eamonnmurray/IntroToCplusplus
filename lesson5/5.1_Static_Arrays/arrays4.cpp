// Example showing how to use a two dimensional array.
#include <iostream>
#include <cmath>
using namespace std;

double matrix_trace(double matrix[3][3]) {
    // Return the trace of a 3x3 matrix.
    double trace = 0.0;

    for (int i = 0; i < 3; ++i) {
        trace += matrix[i][i];
    }

    return trace;
}

void read_matrix(double matrix[3][3]) {
    // Read a 3x3 matrix from stdin.

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            cin >> matrix[i][j];
        }
    }
}

int main() {
    // We can define the dimension as a variable.
    double matrix[3][3];

    read_matrix(matrix);
    cout << matrix_trace(matrix) << endl;

    return 0;
}
