// Example showing how to read a file.
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {

    int count;
    double x, y, x_total, y_total;

    ifstream infile("example.dat");
    if ( ! infile.is_open() ) {
        cout << "Error opening file." << endl;
	return 1;
    }

    x_total = 0.0e0;
    y_total = 0.0e0;
    count = 0;
    // This time we use >> to read numbers from the file.
    // Again we use a while loop to iterate over every line in the file.
    while ( infile >> x >> y ) {
        // Note that >> does not take account of newlines, so if we just had
	// 'infile >> x' then it would iterate over each value on each line.
        x_total += x;
	y_total += y;
	++count;
    }
    infile.close();

    cout << "Read " << count << " values from file." << endl;
    cout << "Average x value: " << x_total / count << endl;
    cout << "Average y value: " << y_total / count << endl;

    return 0;
}
