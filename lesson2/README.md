Introduction to C++: Lesson 2
=============================

Topics
------

1. [Basic Types](#21-basic-types)
2. [Mathematical Operations](#22-mathematical-operations)

2.1 Basic Types
---------------

There are many possible types of variables available in C++. These are
outlined in the following table:


| Name | Description | Size (bytes) | Range |
| ---- | ----------- | ------------ | ----- |
| bool | Boolean value: true or false | 1 | true or false |
| char | A single character (or small integer) | 1 | signed: -128 to 127 <br> unsigned: 0 to 255 |
| short | A short integer | 2 | signed: -32768 to 32767 <br> unsigned: 0 to 65535 |
| int | An integer | 4 | signed: -2147483648 to 2147483647 <br> unsigned: 0 to 4294967295 |
| float | A single-precision floating point number | 4 | +/- 3.4 e +/-38 |
| double | A double-precision floating point number | 8 | +/- 1.7 e +/-308 |

A code with examples of these, and showing what happens when the limits are
exceeded, is given in `2.1_Types\variables_and_types.cpp`:

```c++
// Examples of types and variables.
#include <iostream>
using namespace std;

int main()
{
    bool s;
    char c;
    short a;
    unsigned short b;
    int i;
    unsigned int j;
    float f;
    double d;

    string str;
    // The string type is defined in the std library. Without using the
    // std namespace above we would have to write "std::string str;"

    // bool (1 byte)
    s = false;
    cout << "s (bool, false): " << s << endl;
    s = true;
    cout << "s (bool, true): " << s << endl;
    s = 0;
    cout << "s (bool, 0): " << s << endl;
    s = 1;
    cout << "s (bool, 1): " << s << endl;
    s = 2;
    cout << "s (bool, 2): " << s << endl;
    s = 3;
    cout << "s (bool, 3): " << s << endl;

    // char (1 byte)
    c = 'a'; // Note you need to use single quotes to assign a character.
    cout << "c (char, 'a'): " << c << endl;
    c = 101;
    cout << "c (char, 101): " << c << endl;
    c = 357;
    cout << "c (char, 357): " << c << endl;

    // short (2 bytes)
    a = -5210;
    cout << "a (short, -5210): " << a << endl;
    a = 32767;
    cout << "a (short, 32767): " << a << endl;
    a = 32768;
    cout << "a (short, 32768): " << a << endl;
    b = 32768;
    cout << "b (unsigned short, 32768): " << b << endl;
    b = -100;
    cout << "b (unsigned short, -100): " << b << endl;
    b = 65535;
    cout << "b (unsigned short, 65535): " << b << endl;
    b = 65536;
    cout << "b (unsigned short, 65536): " << b << endl;

    // int (4 bytes)
    i = -2147483648;
    cout << "i (int, -2147483648): " << i << endl;
    i = -2147483649;
    cout << "i (int, -2147483649): " << i << endl;
    j = 4294967295;
    cout << "j (unsigned int, 4294967295): " << j << endl;
    j = 4294967296;
    cout << "j (unsigned int, 4294967296): " << j << endl;

    // float (4 bytes)
    f = 1.0;
    cout << "f (float, 1.0): " << f << endl;
    f = 3.4e30;
    cout << "f (float, 3.4e30): " << f << endl;
    f = 3.4e40;
    cout << "f (float, 3.4e40): " << f << endl;

    // double (8 bytes)
    d = 1.0;
    cout << "d (double, 1.0): " << d << endl;
    d = 3.4e30;
    cout << "d (double, 3.4e30): " << d << endl;
    d = 3.4e300;
    cout << "d (double, 3.4e300): " << d << endl;
    d = 3.4e310;
    cout << "d (double, 3.4e310): " << d << endl;

    // string
    str = "Hello.";
    cout << str << endl;

    // One can read in a value to one of these variable types as before
    cout << "Please enter an integer: " << endl;
    cin >> i;
    cout << "The number you entered was " << i << endl;

    return 0;
}
```

Take a look through the source, then try compiling and running the code.

### [Exercises](../exercises/README.md#21-basic-types)

Please complete the exercises listed in
[section 2.1](../exercises/README.md#21-basic-types) of the file
`exercises/README.md`.

2.2 Mathematical Operations
---------------------------

The basic built in mathematical operations are shown in
`2.2_Operations/operations1.cpp`:

```c++
// Examples of basic mathematical operations.
#include <iostream>
using namespace std;

int main()
{
    int i1, i2, i3;
    // We can initialize several variables of one type separated by commas.

    // +, - and * work as expected.
    cout << "2 + 2 = " << 2 + 2 << endl;
    cout << "1.0 + 9.1 = " << 1.0 + 9.1 << endl;
    cout << "10 - 5 = " << 10 - 5 << endl;
    cout << "1.4 * 1.5 = " << 1.4 * 1.5 << endl;

    // Take care of the type when dividing. Integer division is truncated.
    cout << "3.0 / 4.0 = " << 3.0 / 4.0 << endl;
    cout << "3 / 4. = " << 3 / 4. << endl;
    cout << "3. / 4 = " << 3. / 4 << endl;
    cout << "3 / 4 = " << 3 / 4 << endl;
    cout << "-3 / 4 = " << -3 / 4 << endl;

    // Modulus is represented by %
    cout << "5 % 2 =  " << 5 % 2 << endl;
    // Take care though, as this is really a remainder function rather
    // than the usual mathematical definition.
    cout << "-5 % 2 = " << -5 % 2 << endl;

    i1 = 1;
    i2 = 3;
    // The result of any operation can be stored in a variable.
    i3 = 10 / i2; // = 3
    cout << i3 << endl;
    // Variables can appear both on the left and right of the =.
    i3 = i3 * i2; // = 9
    cout << i3 << endl;

    // *, / and % have a higher precedence than + and -.
    i1 = i3 * 2 + 2 * 0; // = 18
    cout << i1 << endl;

    // Operations of the same precedence are carried out left to right.
    i2 = 7 / 3 * 2 * 3 / 4 % 2; // = 1
    cout << i2 << endl;

    // Compound assignment operators can also be used.
    i1 += 5; // i1 = i1 + 5;
    i2 -= 4; // i2 = i2 - 4;
    i3 *= 2; // i3 = i3 * 2;
    i1 /= 2; // i1 = i1 / 2;
    i2 %= 3; // i2 = i2 % 2;

    // Additionally increment (++) and decrement (--) operators are common.
    ++i1; // i1 = i1 + 1;
    --i2; // i2 = i2 - 1;
    // Note you can also use i1++ and i2--; most of the time they can be used
    // interchangeably, but they do mean slightly different things.
    // Generally it's safest to stick to the ++i1 form.

    return 0;
}
```

To use more advance mathematical functions we can use the `cmath` header, as
shown in `operations2.cpp`:

```c++
// Examples of mathematical operations and functions using cmath.
// Note the extra #include statement here. This makes additional mathematical
// operations available.
#include <cmath>
#include <iostream>
using namespace std;

int main()
{
    cout << "pow(2.4, 2) = " << pow(2.4, 2) << endl; // 2.4^2
    cout << "pow(2.4, 2.4) = " << pow(2.4, 2.4) << endl; // 2.4^2.4
    cout << "exp(2.4) = " << exp(2.4) << endl; // e^2.4
    cout << "log(2.4) = " << log(2.4) << endl; // log(2.4) (natural)
    cout << "sqrt(2.4) = " << sqrt(2.4) << endl; // 2.4^0.5
    cout << "log10(2.4) = " << log10(2.4) << endl; // log10(2.4) (base 10)
    cout << "sin(3.14) = " << sin(3.14) << endl; // NB: Argument in radians.
    // The other trigonmentic functions are available in the same way.
    cout << "abs(-11.4) = " << abs(-11.4) << endl;
    // Many other mathematical functions are available.

    // Care needs to be taken as these functions may not be defined for
    // integer arguments (apart from exponention where integer powers are
    // allowed). It will depend on the C++ standard used by your compiler:
    // the most recent (C++11) will convert the argument to a double if given
    // an integer.
    // Generally, if the function is unambiguous then an integer argument will
    // be implicitly converted to a double.

    // Some compilers will give an error if asked to compile the following:
    // cout << "pow(2, 2) = " << pow(2, 2) << endl;
    //
    // Instead we can explicity convert the first argument to a double.
    // This can be done in one of two equivalent ways.
    cout << "pow( double(2), 2) = " << pow( double(2), 2) << endl;
    cout << "pow( (double) 2, 2) = " << pow( (double) 2, 2) << endl;

    // In general one can do either of:
    // new_type (variable);
    // (new_type) variable;

    return 0;
}
```

You will need to edit the makefile to compile `operations2.cpp`.

### [Exercises](../exercises/README.md#22-mathematical-operations)

Please complete the exercises listed in
[section 2.2](../exercises/README.md#22-mathematical-operations) of the file
`exercises/README.md`.
