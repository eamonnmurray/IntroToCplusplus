// Examples of loops.
// These repeatedly perform a set of statements until a condition is satisfied.
#include <iostream>
using namespace std;

int main() {
    int i;
    string ans;

    // The first type of loop is the while loop.

    // We can also use this to ensure input is valid.
    cout << "Enter y or n followed by return: ";
    cin >> ans;
    while (ans != "y" && ans != "n") {
        cout << "You didn't enter y or n. Enter y or n followed by return: ";
        cin >> ans;
    }

    if (ans == "y") {
        cout << "You entered y." << endl;
    } else {
        cout << "You entered n." << endl;
    }

    // Or we can loop over a range of values.
    cout << "while loop from 1 to 10." << endl;
    i = 1;
    while (i <= 10) {
        cout << "i = " << i << endl;
        i = i + 1;
        // we can assign a value to a variable that depends on its previous
        // value. This increments the value of i by 1.
        // Note this is typically written as "++i" or "i++" which is
        // equivalent.
        // Similarly "--i" is equivalent to "i = i - 1".
    }

    // The second type of loop is the for loop. This allows a more direct
    // way to loop over a set integer range.
    cout << "for loop from 1 to 10." << endl;
    for (i = 1; i <= 10; ++i) {
        cout << "i = " << i << endl;
    }

    cout << "for loop in powers of 2." << endl;
    for (i = 1; i <= 256; i = i * 2) {
        cout << "i = " << i << endl;
    }

    return 0;
}
