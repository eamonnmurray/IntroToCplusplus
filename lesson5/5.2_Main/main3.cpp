// Example showing more features of arguments to main.
// Example usage:
// ./main3.x hello world
#include <iostream>
#include <cstring> // provides strlen.
using namespace std;

int main(int argc, char** argv) {

    int i, j, arg_length;
    char *p;

    cout << "Number of arguments: " << argc << endl;

    for (i = 1; i < argc; ++i) {
        cout << "argument " << i << " = " << argv[i] << endl;
        // Here we use a special behaviour of cout: if given a character
        // pointer it automatically dereferences it and prints until the end
        // of the character string.

        p = argv[i]; // Set pointer equal to pointer of current argument.
        arg_length = strlen(p); // Gets length of character string.

        for (j = 0; j != arg_length; ++j) {
            // Manually derefence p: prints just *p rather than *p to end of
            // string.
            cout << *p << endl;
            ++p;
        }
    }

    return 0;
}
