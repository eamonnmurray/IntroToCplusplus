Introduction to C++: Lesson 8
=============================

Topics
------

1. [Using External Libraries](#81-using-external-libraries),
2. [Classes](#82-classes)

8.1 Using External Libraries
----------------------------

There are many external libraries available to use with C++. These can be used
to provide additional functionality, such as graphical output, or simply to
save you from writing a large chunk of code that someone else has already
written, optimized, compiled and made available to use. There is a large list
of open source C++ libraries at http://en.cppreference.com/w/cpp/links/libs
covering many different uses.

In this example, we'll use the GNU Scientific Library (GSL) which is
compatible with both C and C++ codes. Take a look at the code in
`8.1_External_Libraries/gsl/root.cpp`:
```c++
// Code to show how to call an external library (GSL quadratic root solver).
#include <iostream>
// This loads the interface to the various functions for working with
// polynomials provided by gsl.
#include <gsl/gsl_poly.h>
using namespace std;

int main() {
    int nroots;
    double a, b, c, root1, root2;

    cout << "Enter a, b and c for a quadratic equation (ax^2+bx+c=0): ";
    cin >> a;
    cin >> b;
    cin >> c;

    // Now we can use a gsl function directly in our code. This function takes
    // coefficients of a quadratic equation, along with two pointers to
    // doubles that will be used to store the roots, and returns an integer
    // indicating the number of roots found. Note we need to add
    // '-lgsl -lgslcblas' to LDFLAGS in our Makefile.
    nroots = gsl_poly_solve_quadratic(a, b, c, &root1, &root2);
    if (nroots == 2) {
      cout << " Two real roots found: " << root1 << " " << root2 << endl;
    } else if (nroots == 1) {
      cout << " One real root found: " << root1 << endl;
    } else {
      cout << " No real roots found." << endl;
    }

    return 0;
}
```
As you can see, to use a function provided by an external library, one must
first include the corresponding header. This gives details of the interface.
It's a good idea to check documentation associated with the library to make
sure you're calling it correctly, in particular that you are using the
variable types it expects. The documentation page associated with the
particular GSL function we're using here is at
https://www.gnu.org/software/gsl/manual/html_node/Quadratic-Equations.html
for example.

The linking step of the compilation also needs to be updated to tell the
compiler to link to the library we're using. Since we're using GSL installed
in the standard location, the linking command would be
```bash
g++ root.o -o root.x -lgsl -lgslcblas
```
(the GSL library also needs a blas library which is why both are linked here.)
Since we are using a Makefile, we just need to update one line:
```makefile
LDFLAGS = -lgsl -lgslcblas
```

Note if we were trying to link to a library in a non-standard location, the
directories of both the header file and library would need to be specified at
the compile and link step respectively.
```bash
g++ -c -Wall -Wextra -I/usr/local/include root.cpp -o root.o
g++ root.o -o root.x -L/usr/local/lib -lgsl -lgslcblas
```
If needed, the Makefile could be updated to do this.

There are further details of the GSL at https://www.gnu.org/software/gsl
including a full manual listing all the functions it provides.

The [armadillo C++ linear algebra library](http://arma.sourceforge.net) has
also been installed on your Xubuntu systems. This provides many custom types
and functions for calculations involving vectors and matrices. An example is
given in `8.1_External_Libraries/armadillo/cross/cross.cpp`:
```c++
// Code to show how to use the armadillo library.
#include <iostream>
// This loads the interface to the various functions for working with
// the armadillo library
#include <armadillo>

using namespace std;
using namespace arma;
// Note: we also load the arma namespace.

int main() {
    vec a(3), b(3), c(3);
    // This is the armadillo vector defined type that specifies a column
    // vector of doubles. Note we use parentheses rather than brackets when
    // working with this armadillo vector type.

    cout << "Enter vectors a and b (3 elements each): ";
    cin >> a(0) >> a(1) >> a(2);
    cin >> b(0) >> b(1) >> b(2);

    c = cross(a, b);
    // This is an armadillo defined function for the vector cross product.

    cout << "a X b = " << c.t() << endl;
    // Note the use of t() here which takes the transpose. This turns our
    // column vector into a row vector so the output will be on one line.

    return 0;
}
```

A second example using armadillo to calculate the determinant of a matrix
stored in a file is givenin `8.1_External_Libraries/armadillo/det/det.cpp`:
```c++
// Code to show how to use the armadillo library to calculate the determinant
// of a matrix.
#include <iostream>
#include <armadillo>

using namespace std;
using namespace arma;

int main() {
    mat A;
    // Here we use the armadillo matrix defined type that specifies a matrix
    // of doubles. Note we don't need to specify the dimensions.

    A.load("A.dat", raw_ascii);
    // This will read the values stored in the file A.dat into the matrix A.
    cout << det(A) << endl;
    // Now we output the result of the armadillo "det" function.

    return 0;
}
```
This clearly shows how much simpler it can be to use an external library
rather than trying to reinvent the wheel when writing code involving common
mathematical operations.

### [Exercises](../exercises/README.md#81-using-external-libraries)

Please complete the exercises listed in
[section 8.1](../exercises/README.md#81-using-external-libraries) of the file
`exercises/README.md`.

8.2 Classes
-----------

Classes in C++ are similar to data structures, but as well as storing data
they can also store functions. The other difference is that elements of
a class are all **private** by default. This means that unless otherwise
specified, variables defined within a class are only accessible to functions
defined within the class. The `public:` keyword can be used to indicate
variables should be public. For example, the following code, in
`8.2_Classes/classes1.cpp` is equivalent to the example given in the
data structures section above.

```c++
// Example showing how to use classes.
#include <iostream>
#include <string>

using namespace std;

// We use the following construction to create a class named
// Atom, containing several different types of variables, all marked
// as public.
class Atom {
    public:
        int atomic_number;
        double mass_number;
        double radius_pm;
        string symbol;
} ;


// This function will output the information stored in the structure.
// variables contained in the structure are accessed using the
// structure_name.variable_name construction.
void output_atom_info(Atom a) {
    cout << a.symbol << endl;
    cout << "Atomic number: " << a.atomic_number << endl;
    cout << "Mass number: " << a.mass_number << endl;
    cout << "Atomic radius (pm): " << a.radius_pm << endl;
}

int main() {
    // Initialize two objects of the Atom class.
    Atom hydrogen, lithium;

    // Since the class variables are public we can set them directly.
    hydrogen.atomic_number = 1;
    hydrogen.mass_number = 1.008;
    hydrogen.symbol = "H";
    hydrogen.radius_pm =  25;

    lithium.atomic_number = 3;
    lithium.mass_number = 6.941;
    lithium.symbol = "Li";
    lithium.radius_pm = 145;

    output_atom_info(hydrogen);
    cout << endl;
    output_atom_info(lithium);

    return 0;
}
```

When using a class we can also incorporate the function into the class. We
modify the above example to do this in `classes2.cpp`:

```c++
// Example showing how to use classes containing functions.
#include <iostream>
#include <string>

using namespace std;

class Atom {
    public:
        int atomic_number;
        double mass_number;
        double radius_pm;
        string symbol;

        // We add a function here that will output the data stored in the
        // class. Note we don't need an argument here, since the function
        // has access to the class variables directly. Note this function
        // is also public, so we can call it from the main function.
        void output_info() {
            cout << symbol << endl;
            cout << "Atomic number: " << atomic_number << endl;
            cout << "Mass number: " << mass_number << endl;
            cout << "Atomic radius (pm): " << radius_pm << endl;
        }
} ;

int main() {
    // Initialize two objects of the Atom class.
    Atom hydrogen, lithium;

    // Since the variables are public, we can set the variables contained in
    // each directly.
    hydrogen.atomic_number = 1;
    hydrogen.mass_number = 1.008;
    hydrogen.symbol = "H";
    hydrogen.radius_pm =  25;

    lithium.atomic_number = 3;
    lithium.mass_number = 6.941;
    lithium.symbol = "Li";
    lithium.radius_pm = 145;

    // The following construction calls the class function to output the data.
    hydrogen.output_info();
    cout << endl;
    lithium.output_info();

    return 0;
}
```

If we didn't specify the class variables as public, we could use another
function to set them. This is shown in `classes3.cpp`. Note in this example
we also show another way to define a function in a class:

```c++
// Example showing how to set private variables in a class.
#include <iostream>
#include <string>

using namespace std;

class Atom {
    // These variables are all now private. Here we're also using a naming
    // convention where private variables end in '_'.
    int atomic_number_;
    double mass_number_;
    double radius_pm_;
    string symbol_;

    public:
        // This function is able to output the private variables in the class
        // since it is also contained in the class.
        void output_info() {
            cout << symbol_ << endl;
            cout << "Atomic number: " << atomic_number_ << endl;
            cout << "Mass number: " << mass_number_ << endl;
            cout << "Atomic radius (pm): " << radius_pm_ << endl;
        }

        // Instead of defining the full function, we can define the interface
        // and define the function later.
        void set_info(string, int, double, double);
} ;

// Note the syntax here to define the function contained in a class.
void Atom::set_info(string s, int a, double m, double r) {

    symbol_ = s;
    atomic_number_ = a;
    mass_number_ = m;
    radius_pm_ = r;
}

int main() {
    // Initialize two objects of the Atom class.
    Atom hydrogen, lithium;

    hydrogen.set_info("H", 1, 1.008, 25);
    lithium.set_info("Li", 3, 6.941, 145);

    hydrogen.output_info();
    cout << endl;
    lithium.output_info();

    return 0;
}
```

It's common to also define classes in header files. In this case, the class
definition should contain the function interfaces and the functions themselves
should be in a separate `.cpp` file. An example of this is given in the
directory `roots`. First we define the class in `quad.h`:
```c++
// Define the Quad class.
class Quad {
    public:
        // These correspond to the parameters for ax^2+bx+c=0.
        double a, b, c;

        // We include two function interfaces.
        double root1();
        double root2();
} ;
```
Then we define the associated functions in `quad.cpp`:
```c++
#include <cmath>
// Note we need to include the header here so that this source file can access
// to the Quad class definition.
#include "quad.h"
using namespace std;

double Quad::root1() {
    // Return the larger root of a quadratic equation.

    double root;

    root = -b + sqrt(pow(b, 2) - 4.0 * a * c);
    root = root / (2.0 * a);
    return root;
}

double Quad::root2() {
    // Return the smaller root of a quadratic equation.

    double root;

    root = -b - sqrt(pow(b, 2) - 4.0 * a * c);
    root = root / (2.0 * a);
    return root;
}
```
And we can now use this with the main code in `root.cpp` as follows:
```c++
// An example of the use of a class that is defined in a separate file using
// a header file.
#include <iostream>
// Again we must include the header file here.
#include "quad.h"
using namespace std;

int main() {
    Quad eqn;

    cout << "Enter a, b and c for a quadratic equation (ax^2+bx+c=0): ";
    cin >> eqn.a;
    cin >> eqn.b;
    cin >> eqn.c;

    cout << eqn.root1() << " " << eqn.root2() << endl;

    return 0;
}
```

### [Exercises](../exercises/README.md#82-classes)

Please complete the exercises listed in
[section 8.2](../exercises/README.md#82-classes) of the file
`exercises/README.md`.

