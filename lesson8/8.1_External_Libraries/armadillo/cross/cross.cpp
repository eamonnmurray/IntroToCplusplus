// Code to show how to use the armadillo library.
#include <iostream>
// This loads the interface to the various functions for working with
// the armadillo library
#include <armadillo>

using namespace std;
using namespace arma;
// Note: we also load the arma namespace.

int main() {
    vec a(3), b(3), c(3);
    // This is the armadillo vector defined type that specifies a column
    // vector of doubles. Note we use parentheses rather than brackets when
    // working with this armadillo vector type.

    cout << "Enter vectors a and b (3 elements each): ";
    cin >> a(0) >> a(1) >> a(2);
    cin >> b(0) >> b(1) >> b(2);

    c = cross(a, b);
    // This is an armadillo defined function for the vector cross product.

    cout << "a X b = " << c.t() << endl;
    // Note the use of t() here which takes the transpose. This turns our
    // column vector into a row vector so the output will be on one line.

    return 0;
}
