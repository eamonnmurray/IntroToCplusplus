// Define the Quad class.
class Quad {
    public:
        // These correspond to the parameters for ax^2+bx+c=0.
        double a, b, c;

        // We include two function interfaces.
        double root1();
        double root2();
} ;
